import moment from 'moment';
import querystring from 'query-string';
import { config } from '../config';

// Crate line flex message before send to user
export async function createFlexMessage(location) {
  const startAddress = location.routes[0].legs[0].start_address
  const endAddress = location.routes[0].legs[0].end_address
  const distance = location.routes[0].legs[0].distance.text
  const duration = location.routes[0].legs[0].duration.text
  const durationValue = location.routes[0].legs[0].duration.value
  const summary = location.routes[0].summary;
  const startTime = moment().format("HH:mm");
  const endTime = moment().seconds(durationValue).format("HH:mm");
  const originLatLng = `${location.routes[0].legs[0].start_location.lat},${location.routes[0].legs[0].start_location.lng}`;
  const destinationLatLng = `${location.routes[0].legs[0].end_location.lat},${location.routes[0].legs[0].end_location.lng}`;
  const query = {
    api: 1,
    origin: originLatLng,
    destination: destinationLatLng,
    travelmode: 'driving',
    key: config.service.googleAppKey,
  };
  // link for redirect google map
  const googleApiUrl = `https://www.google.com/maps/dir/?${querystring.stringify(query)}`;
  // create flex header
  const header = {
    type: 'box',
    layout: 'vertical',
    contents: [
      {
        type: 'box',
        layout: 'vertical',
        contents: [
          {
            type: 'text',
            text: 'FROM',
            color: '#ffffff66',
            size: 'sm'
          },
          {
            type: 'text',
            text: `${startAddress}`,
            color: '#ffffff',
            size: 'lg',
            flex: 4,
            weight: 'bold',
            wrap: true
          }
        ]
      },
      {
        type: 'box',
        layout: 'vertical',
        contents: [
          {
            type: 'text',
            text: 'TO',
            color: '#ffffff66',
            size: 'sm'
          },
          {
            type: 'text',
            text: `${endAddress}`,
            color: '#ffffff',
            size: 'lg',
            flex: 4,
            weight: 'bold',
            wrap: true
          }
        ]
      }
    ],
    paddingAll: '20px',
    backgroundColor: '#0367D3',
    spacing: 'md',
    paddingTop: '22px'
  }
  // create flex body
  const body = {
    type: 'box',
    layout: 'vertical',
    contents: [
      {
        type: 'text',
        text: `ใช้เวลา: ${duration}`,
        size: 'xs'
      },
      {
        type: 'text',
        text: `${summary}`,
        color: '#b7b7b7',
        size: 'xs'
      },
      {
        type: 'box',
        layout: 'horizontal',
        contents: [
          {
            type: 'text',
            text: `${startTime}`,
            size: 'sm',
            gravity: 'center'
          },
          {
            type: 'box',
            layout: 'vertical',
            contents: [
              {
                type: 'filler'
              },
              {
                type: 'box',
                layout: 'vertical',
                contents: [
                  {
                    type: 'filler'
                  }
                ],
                cornerRadius: '30px',
                height: '12px',
                width: '12px',
                borderColor: '#EF454D',
                borderWidth: '2px'
              },
              {
                type: 'filler'
              }
            ],
            flex: 0
          },
          {
            type: 'text',
            text: 'Start',
            gravity: 'center',
            flex: 4,
            size: 'sm'
          }
        ],
        spacing: 'lg',
        cornerRadius: '30px',
        margin: 'xl'
      },
      {
        type: 'box',
        layout: 'horizontal',
        contents: [
          {
            type: 'box',
            layout: 'baseline',
            contents: [
              {
                type: 'filler'
              }
            ],
            flex: 1
          },
          {
            type: 'box',
            layout: 'vertical',
            contents: [
              {
                type: 'box',
                layout: 'horizontal',
                contents: [
                  {
                    type: 'filler'
                  },
                  {
                    type: 'box',
                    layout: 'vertical',
                    contents: [
                      {
                        type: 'filler'
                      }
                    ],
                    width: '2px',
                    backgroundColor: '#B7B7B7'
                  },
                  {
                    type: 'filler'
                  }
                ],
                flex: 1
              }
            ],
            width: '12px'
          },
          {
            type: 'text',
            text: `ระยะทาง ${distance}`,
            gravity: 'center',
            flex: 4,
            size: 'xs',
            color: '#8c8c8c'
          }
        ],
        spacing: 'lg',
        height: '64px'
      },
      {
        type: 'box',
        layout: 'horizontal',
        contents: [
          {
            type: 'text',
            text: `${endTime}`,
            gravity: 'center',
            size: 'sm'
          },
          {
            type: 'box',
            layout: 'vertical',
            contents: [
              {
                type: 'filler'
              },
              {
                type: 'box',
                layout: 'vertical',
                contents: [
                  {
                    type: 'filler'
                  }
                ],
                cornerRadius: '30px',
                width: '12px',
                height: '12px',
                borderColor: '#6486E3',
                borderWidth: '2px'
              },
              {
                type: 'filler'
              }
            ],
            flex: 0
          },
          {
            type: 'text',
            text: 'End',
            gravity: 'center',
            flex: 4,
            size: 'sm'
          }
        ],
        spacing: 'lg',
        cornerRadius: '30px'
      }
    ]
  }
  // create flex footer
  const footer = {
    type: 'box',
    layout: 'vertical',
    contents: [
      {
        type: 'button',
        action: {
          type: 'uri',
          label: 'นำทาง',
          uri: `${googleApiUrl}`
        }
      }
    ],
    backgroundColor: '#e9ebee'
  }
  // build flex message result
  const resultMessage = {
    type: 'flex',
    altText: "เส้นทางสำหรับคุณ",
    contents: {
      type: "bubble",
      size: "giga",
      header,
      body,
      footer
    }
  }
  
  return resultMessage;
}