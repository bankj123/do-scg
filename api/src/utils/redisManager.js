import { promisify } from 'util';
import * as redis from 'redis';
import { config } from '../config';

export class RedisManager {
  constructor() {
    this.redisClient = undefined;
    this._getAsync = undefined;
    this._setAsync = undefined;
    this._setExAsync = undefined;
  }

  // Connect Redis
  async connectRedis() {
    const client = redis.createClient({
      host: config.redis.host,
      port: config.redis.port,
      password: config.redis.password,
      retry_strategy: function (options) {
        console.log('retrying');
        return Math.min(options.attempt * 100, 3000);
      }
    });
    return client;
  }

  // Disconnect Redis
  async disconnectRedis() {
    if (this.redisClient) {
      this.redisClient.quit();
    }
  }

  // Connect redis before use data
  async getRedisClient() {
    if (this.redisClient) {
      return this.redisClient;
    }
    this.redisClient = await this.connectRedis();
    this._getAsync = promisify(this.redisClient.get).bind(this.redisClient);
    this._setAsync = promisify(this.redisClient.set).bind(this.redisClient);
    this._setExAsync = promisify(this.redisClient.setex).bind(this.redisClient);
    return this.redisClient;
  }

  // Get data redis by key
  async get(key) {
    await this.getRedisClient();
    return await this._getAsync(key);
  }

  // Set data redis by key
  async set(key, value) {
    await this.getRedisClient();
    return await this._setAsync(key, value);
  }

  // Set dat redis and expire redis by key
  async setEx(key, expiry, value) {
    await this.getRedisClient();
    return await this._setExAsync(key, expiry, value);
  }

}
const Redis = new RedisManager();
export default Redis;
