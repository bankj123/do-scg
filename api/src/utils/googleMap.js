import axios from 'axios';
import querystring from 'querystring';
import { config } from '../config';

export class GoogleMap {
  constructor() {
    this.httpReq = axios.create({
      validateStatus: status => {
        return true;
      }
    });
  }

  async get(url) {
    const response = await this.httpReq.get(url);
    return response.data;
  }

  async getBestWay(origin, destination, mode) {
    const query = {
      origin,
      destination,
      region: 'th',
      language: 'th',
      mode,
      key: config.service.googleAppKey
    };
    const googleApiUrl = `https://maps.googleapis.com/maps/api/directions/json?${querystring.stringify(query)}`;
    return this.get(googleApiUrl);
  }
}
const GMap = new GoogleMap();
export default GMap;
