
import rq from 'request';
import { config } from '../config';

// Send Notification to line
export class Notification {
  async send(message) {
    rq({
      method: 'POST',
      uri: 'https://notify-api.line.me/api/notify',
      header: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      auth: {
        bearer: `${config.line.notifyToken}`, //token
      },
      form: {
        message, 
      },
    }, (err, httpResponse, body) => {
      if (err) {
        console.log(err)
      } else {
        console.log(body)
      }
    })
  }
}
const Notify = new Notification();
export default Notify;
