import express from 'express';
import { resp } from '../utils/common';
import { config } from '../config';
import GoogleMap from '../utils/googleMap';
import RedisManager from '../utils/redisManager';
import { createFlexMessage } from '../utils/lineMessageBuilder';
import Notification from '../utils/lineNotification';
const line = require('@line/bot-sdk');
const client = new line.Client(config.line);

export const router = express.Router();

router.post('/', webhook);

// handle events type form line
async function handleEvent(event) {
  switch (event.type) {
    case 'message':
      const message = event.message;
      switch (message.type) {
        case 'text': // case input text message
          return handleText( message, event);
        case 'location': // case input location
          return await handleLocation(message, event);
        default: // other input type
          throw new Error(`Unknown message: ${JSON.stringify(message)}`);
      }
    case 'follow': // case follow 
      const profile = await client.getProfile(event.source.userId);
      const followText = `สวัสดีคุณ ${profile.displayName} ยินดีต้อนรับครับ`;
      return client.replyMessage(event.replyToken, { type: 'text', text: followText });

    case 'unfollow': // case unfollow 
      return console.log(`Unfollowed this bot: ${JSON.stringify(event)}`);
    default:
      return true;
  }
}

// handle input message text form line
async function handleText(message, event) {
  // get profile line
  const profile = await client.getProfile(event.source.userId);
  // set time send warning notification before 10 sec
  let timerNotify = setTimeout(() => {
    Notification.send(`บอทไม่สามารถตอบโต้คุณ ${profile.displayName} เกิน 10 วินาที`);
  }, 10 * 1000);
  const reg= /SCG/ig; 
  if (reg.test(message.text.toUpperCase())) {
    // clear warning notification
    clearTimeout(timerNotify);
    return client.replyMessage(event.replyToken, { type: 'text', text: 'กรุณาส่ง Location เพื่อหาทางไป Digital Office SCG' });
  } else {
    return client.replyMessage(event.replyToken, { type: 'text', text: 'กรุณารอสักครู่' });
  }
}

// handle input location form line
async function handleLocation(message, event) {
  const origin = `${message.latitude.toFixed(3)},${message.longitude.toFixed(3)}`;
  // convert key for redis
  const key = origin.replace(/[,.]/g, val => (val === ',' ? '_' : '-'))
  const destination = 'SCG';
  let location = await RedisManager.get(`Location_${key}_${destination}`);
  if (location) {
    location = JSON.parse(location)
  } else {
    // find best way google map api directions
    location = await GoogleMap.getBestWay(origin, destination, 'driving');
    // set data redis expire (10 min)
    await RedisManager.setEx(`Location_${key}_${destination}`, 600, JSON.stringify(location));
  }
  // build flex message
  const flexMessage = await createFlexMessage(location);
  return client.replyMessage(event.replyToken, flexMessage);
}

export async function webhook(req, res, next) {
  if (!Array.isArray(req.body.events)) {
    return res.status(500).end();
  }
  // handle events separately
  return Promise.all(req.body.events.map((event) => {
    // check verify webhook event
    if (event.replyToken === '00000000000000000000000000000000' ||
      event.replyToken === 'ffffffffffffffffffffffffffffffff') {
      return true;
    }
    return handleEvent(event);
  }))
    .then(() => next(resp({ data: 'OK' }, 200)))
    .catch((err) => {
      next(resp({ message: err.message }, 400));
    });
}
