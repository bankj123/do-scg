import express from 'express';
import { ObjectId } from 'mongodb';
import { resp } from '../utils/common';
import RedisManager from '../utils/redisManager';

const Doscg = require("../models/Doscg");

export const router = express.Router();

router.get('/', getDoscgList);
router.get('/:id', getDoscgById);
router.post('/', createDoscg);
router.put('/:id', updateDoscg);
router.delete('/:id', deleteDoscg);

// Get SCG Digital office project list
export async function getDoscgList(req, res, next) {
  try {
    const result = await Doscg.find();
    next(resp({ data: result }));
  } catch (err) {
    next(resp({ message: err.message }, 400));
  }
}

// Get SCG Digital office project By ID
export async function getDoscgById(req, res, next) {
  try {
    const id = req.params.id;
    let cache = await RedisManager.get(`Doscg_${id}`);
    if (cache) {
      cache = JSON.parse(cache)
      next(resp({ data: cache }));
    }
    const result = await Doscg.findOne({ _id: ObjectId(id) });
    // set data redis expire (10 min)
    await RedisManager.setEx(`Doscg_${id}`, 600, JSON.stringify(result));
    next(resp({ data: result }));
  } catch (err) {
    next(resp({ message: err.message }, 400));
  }
}

// Create SCG Digital office project
export async function createDoscg(req, res, next) {
  try {
    let doscg = new Doscg(req.body);
    await doscg.save();
    next(resp({ success: true, data: doscg }));
  } catch (err) {
    next(resp({ message: err.message }, 400));
  }
}

// Update SCG Digital office project
export async function updateDoscg(req, res, next) {
  try {
    const id = req.params.id;
    let data = req.body;
    delete data._id;
    data.updated_at = new Date();

    const result = await Doscg.findOneAndUpdate({
      _id: ObjectId(id),
    }, { $set: data }, { returnNewDocument: true });
    next(resp({ success: true, data: result }));
  } catch (err) {
    next(resp({ message: err.message }, 400));
  }
}

// Delete SCG Digital office project
export async function deleteDoscg(req, res, next) {
  try {
    let id = req.params.id;
    await Doscg.findOneAndDelete({ _id: ObjectId(id) });
    next(resp({ success: true, data: { _id: id } }));
  } catch (error) {
    next(resp({ error }, 400));
  }
}
