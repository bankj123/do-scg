import express from 'express';
import { resp } from '../utils/common';
import GoogleMap from '../utils/googleMap';
import RedisManager from '../utils/redisManager';

export const router = express.Router();

router.get('/find', findData);
router.get('/missing', missData);
router.post('/best-way', bestWay);

// Find Data X Y Z in data list
export async function findData(req, res, next) {
  try {
    const question = ['X', 'Y', 5, 9, 15, 23, 'Z'];
    const data = [5, 9, 15, 23];
    const differenceList = [];
    // find difference list in value
    data.forEach((value, index) => {
      if (index < data.length - 1) {
        differenceList.push(data[index + 1] - value);
      }
    });
    let difference;
    // check pattern in difference list
    differenceList.forEach((value, index) => {
      if (index < differenceList.length - 1) {
        const differenceTemp = differenceList[index + 1] - value;
        if (!index || difference === differenceTemp) {
          difference = differenceTemp;
        } else {
          throw new Error('No pattern');
        }
      }
    });
    // set data first , second , last
    const second = data[0] - (differenceList[0] - difference);
    const first = second - (differenceList[0] - difference - difference);
    const last =
      data[data.length - 1] +
      differenceList[differenceList.length - 1] +
      difference;
    const result = data;
    result.unshift(second);
    result.unshift(first);
    result.push(last);
    next(resp({ question, result }));
  } catch (err) {
    next(resp({ message: err.message }, 400));
  }
}

// Find Data B and C in logic A + B = 23 , A + C = -21
export async function missData(req, res, next) {
  try {
    // get A by query string default by 21
    const A = +req.query.a || 21;
    const answerB = 23;
    const answerC = -21;
    const question = `A = ${A}, A + B = ${answerB}, A + C = ${answerC}`;
    const B = +answerB - +A;
    const C = +answerC - +A;
    next(resp({ question, result: { A, B, C } }));
  } catch (err) {
    next(resp({ message: err.message }, 400));
  }
}

// Find best way between location
export async function bestWay(req, res, next) {
  try {
    const { body } = req;
    const origin = body.origin;
    const destination = body.destination;
    const keyOrigin = origin.replace(/ /g, "-");
    const keyDestination = destination.replace(/ /g, "-");
    // driving || walking || bicycling || transit
    const mode = body.mode || 'driving';
    let cache = await RedisManager.get(`Location_${keyOrigin}_${keyDestination}`);
    // check data in redis
    if (cache) {
      cache = JSON.parse(cache)
      // return data cache
      next(resp({ data : cache }));
    }
    // find best way google map api directions
    const location = await GoogleMap.getBestWay(origin, destination, mode);
    // set redis expire (10 min)
    await RedisManager.setEx(`Location_${keyOrigin}_${keyDestination}`, 600, JSON.stringify(location));
    next(resp({ data : location }));
  } catch (err) {
    next(resp({ message: err.message }, 400));
  }
}

