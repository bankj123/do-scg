import mongoose from 'mongoose';

// Model SCG Digital Office
const doscgSchema = mongoose.Schema({
  title: {
    type: String,
    index: true,
    unique: false,
    required: true
  },
  status: {
    type: Number,
    required: true
  },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date }
});
const doscg = mongoose.model('doscg', doscgSchema);
module.exports = doscg;