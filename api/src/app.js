import express from 'express';
import cors from 'cors';
import helmet from 'helmet';
import bodyParser from 'body-parser';
import { router as doscg } from './routers/doscg';
import { router as logic } from './routers/logic';
import { router as line } from './routers/line';
const prefixApi = '';
// Connect MongoDB
const mongoose = require("mongoose");
mongoose.connect("mongodb://scg_mongodb/doscg");
const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error:"));
db.once("open", () => {
  console.log("Connected MongoDB");
});
const app = express();
app.use(cors());
app.use(helmet());
// Body Parser middleware
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(`${prefixApi}/v1/doscg`, doscg);
app.use(`${prefixApi}/v1/logic`, logic);
app.use(`${prefixApi}/v1/line`, line);

app.use(([body, status], req, res, next) => {
  res.status(status).json(body);
  next();
});
// Specifying the listening port
app.listen(8081, () => {
  console.log('Listening on port 8081')
})