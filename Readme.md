# ![Assignment SCG Digital Office]

Project demo is available at https://scg.justcode.club

This codebase was created to demonstrate a fully fledged fullstack application built with Vue.js including CRUD operations, routing, and more for SCG Digital Office.

## Getting Started

Install API , Redis and MongoDB by Docker Compose

```bash
# build api server , redis and mongoDB by docker-compose
> cd ./api && docker-compose up -d
```

Install Web

```bash
> cd ./web
# install dependencies
> npm install
# serve with hot reload at localhost:8080
> npm start
```

Other commands available are:

```bash
# build for production with minification
> npm run build
```

## To know

Current arbitrary choices are:

- Vuex modules for store
- axios for ajax requests
- Bootstrap V4 for css


## Authors

* **Chayakorn Jenpitak**