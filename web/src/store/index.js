import Vue from 'vue'
import Vuex from 'vuex'
import scg from './scg'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: { scg }
})
