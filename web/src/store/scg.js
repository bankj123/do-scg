// store.js

import Vue from 'vue'

import axios from 'axios'
import { API_URL } from '@/common/config'

export default {
  state: {
    projects: [],
    question1: {},
    question2: {},
    bestway: {}
  },

  mutations: {
    fetchQuestion1 (state, { res }) {
      state.question1 = res.data
    },
    fetchQuestion2 (state, { res }) {
      state.question2 = res.data
    },
    findBestWay (state, { res }) {
      state.bestway = res.data.data
    },
    fetchDoScg (state, { res }) {
      state.projects = res.data.data
    },
    addDoScg (state, { res }) {
      state.projects.push(res.data.data)
    },
    deleteDoScg (state, { payload }) {
      state.projects.splice(payload.index, 1)
      Vue.swal('Deleted!', 'Your file has been deleted.', 'success')
    },
    editDoScg (state, { payload }) {
      state.projects[payload.index].title = payload.title
      state.projects[payload.index].status = +payload.status
    }
  },

  actions: {
    async fetchQuestion1 ({ commit }) {
      await axios.get(`${API_URL}logic/find`)
        .then(res => commit('fetchQuestion1', { res }))
        .catch(err => Vue.swal('Error', err.message, 'error'))
    },
    async fetchQuestion2 ({ commit }) {
      await axios.get(`${API_URL}logic/missing`)
        .then(res => commit('fetchQuestion2', { res }))
        .catch(err => Vue.swal('Error', err.message, 'error'))
    },
    async findBestWay ({ commit }, payload) {
      await axios.post(`${API_URL}logic/best-way`, payload)
        .then((res) => commit('findBestWay', { res }))
        .catch(err => Vue.swal('Error', err.message, 'error'))
    },
    async fetchDoScg ({ commit }) {
      await axios.get(`${API_URL}doscg`)
        .then(res => commit('fetchDoScg', { res }))
        .catch(err => Vue.swal('Error', err.message, 'error'))
    },
    async addDoScg ({ commit }, payload) {
      await axios.post(`${API_URL}doscg`, payload)
        .then((res) => commit('addDoScg', { res }))
        .catch(err => Vue.swal('Error', err.message, 'error'))
    },
    async deleteDoScg ({ commit }, payload) {
      Vue.swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then(async (result) => {
        if (result.value) {
          await axios.delete(`${API_URL}doscg/` + payload._id)
            .then(() => commit('deleteDoScg', { payload }))
            .catch(err => Vue.swal('Error', err.message, 'error'))
        }
      })
    },
    async editDoScg ({ commit }, payload) {
      await axios.put(`${API_URL}doscg/` + payload._id, payload)
        .then(() => commit('editDoScg', { payload }))
        .catch(err => Vue.swal('Error', err.message, 'error'))
    }
  },

  getters: {
    projects: state => state.projects,
    question1: state => state.question1,
    question2: state => state.question2,
    bestway: state => state.bestway
  }
}
