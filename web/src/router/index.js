import Vue from 'vue'
import Router from 'vue-router'
import Question from '../pages/question/Main.vue'
import ScgDo from '../pages/scgdo/Main.vue'
import Cv from '../pages/cv/Main.vue'
import Layout from '../layout/Layout.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: Layout,
      redirect: '/home',
      children: [
        {
          path: 'home',
          name: 'Home',
          component: ScgDo
        },
        {
          path: 'question',
          name: 'Question',
          component: Question
        },
        {
          path: 'cv',
          name: 'CV',
          component: Cv
        }
      ]
    }
  ]
})
