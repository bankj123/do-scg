// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
// Bootstrap plugin
import Bootstrap from './bootstrap-main'
import store from './store'
// Sweetalert2 plugin
import VueSweetalert2 from 'vue-sweetalert2'
import 'sweetalert2/dist/sweetalert2.min.css'

// Axios plugin
import VueAxios from 'vue-axios'
import axios from 'axios'

// fontawesome plugin
import { library } from '@fortawesome/fontawesome-svg-core'
import { faVial, faChalkboard, faMobileAlt, faServer, faLayerGroup, faChevronRight } from '@fortawesome/free-solid-svg-icons'
import { faJs, faVuejs, faGit, faPhp, faLaravel, faReact, faUbuntu, faAws, faCodepen, faSwift, faEnvira, faLine, faUikit, faBootstrap, faHtml5, faCss3, faDocker, faNeos } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faJs, faVuejs, faGit, faPhp, faLaravel, faReact, faUbuntu, faVial, faChalkboard, faAws, faCodepen, faMobileAlt, faSwift, faServer, faEnvira, faLayerGroup, faLine, faUikit, faBootstrap, faHtml5, faCss3, faDocker, faNeos, faChevronRight)
Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.use(VueAxios, axios)
Vue.use(Bootstrap)
Vue.use(VueSweetalert2)
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  store,
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
