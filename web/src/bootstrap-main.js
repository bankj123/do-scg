import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue'

// asset imports
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
/**
 * This is the main Light Bootstrap Dashboard Vue plugin where dashboard related plugins are registerd.
 */
export default {
  install (Vue) {
    Vue.use(BootstrapVue)
    Vue.use(BootstrapVueIcons)
  }
}
